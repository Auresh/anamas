A Not Annoying Media Ansible Service

Getting started
Generate an ssh key pair for the ansible user.
Place the keys pair in root for your directory :

├── ansible.cfg
├── id_rsa
├── id_rsa.pub
├── inventories

Change in inventorys/integration/group_vars/server :
1. ansible_ssh_key path
2. lucks_passwd
